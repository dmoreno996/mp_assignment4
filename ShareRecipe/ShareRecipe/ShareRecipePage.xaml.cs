﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ShareRecipe
{
    public partial class ShareRecipePage : ContentPage
    {
        public ShareRecipePage()
        {
            InitializeComponent();
            PopulateListView();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            templateMaker itemTapped = (templateMaker)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        private void PopulateListView()
        {
            var listOfInts = new ObservableCollection<templateMaker>()
            {
                new templateMaker
                {
                    playerName = "Manchester United Football Club",
                    playerInfo = "Established : 1878 \tGround : Old Trafford",
                    url = "http://www.manutd.com",
                    IconSource = ImageSource.FromFile("logo.png"),

                },
                new templateMaker
                {
                    playerName =  "David De Gea #1",
                    playerInfo = "Pos : Goalkeeper \tAge: 27 \tCountry : Spain",
                    url = "http://www.futhead.com/18/players/49489/de-gea/",
                    IconSource = ImageSource.FromFile("ddg.png"),

                },
                new templateMaker
                {
                    playerName = "Sergio Romero #20",
                    playerInfo = "Pos : Goalkeeper \tAge: 30 \tCountry : Argentina",
                    url = "http://www.futhead.com/18/players/917/sergio-romero/",
                    IconSource = ImageSource.FromFile("romero.png"),

                },
                new templateMaker
                {
                    playerName = "Joel Pereira #40",
                    playerInfo = "Pos : Goalkeeper \tAge: 21 \tCountry : Portugal",
                    url = "http://www.futhead.com/18/players/6713/joel-pereira/ddg",
                    IconSource = ImageSource.FromFile("joel.png"),

                },
//Defenders-----------------------------------------------------------------------------------
                new templateMaker
                {
                    playerName = "Eric Bailly #3",
                    playerInfo = "Pos : Centerback \tAge: 23 \tCountry : Ivory Coast",
                    url = "http://www.futhead.com/18/players/5543/eric-bailly/",
                    IconSource = ImageSource.FromFile("bailly.png"),

                },                
                new templateMaker
                {
                    playerName = "Marcos Rojo #5",
                    playerInfo = "Pos : Centerback \tAge: 27 \tCountry : Argentina",
                    url = "http://www.futhead.com/18/players/4049/marcos-rojo/",
                    IconSource = ImageSource.FromFile("rojo.png"),

                },
                new templateMaker
                {
                    playerName = "Chris Smalling #12",
                    playerInfo = "Pos : Centerback \tAge: 27 \tCountry : England",
                    url = "http://www.futhead.com/18/players/1988/chris-smalling/",
                    IconSource = ImageSource.FromFile("smalling.png"),

                },
                new templateMaker
                {
                    playerName = "Phil Jones #4",
                    playerInfo = "Pos : Centerback \tAge: 25 \tCountry : England",
                    url = "http://www.futhead.com/18/players/6857/phil-jones/",
                    IconSource = ImageSource.FromFile("jones.png"),

                },
                new templateMaker
                {
                    playerName = "Victor Lindelöf #",
                    playerInfo = "Pos : Centerback \tAge: 23 \tCountry : Portugal",
                    url = "http://www.futhead.com/18/players/8049/victor-lindelof/",
                    IconSource = ImageSource.FromFile("victor.png"),

                },
                new templateMaker
                {
                    playerName = "Antonia Valencia #",
                    playerInfo = "Pos : Rightback \tAge: 32 \tCountry : Ecuador",
                    url = "http://www.futhead.com/18/players/1817/antonio-valencia/",
                    IconSource = ImageSource.FromFile("valencia.png"),

                },
                new templateMaker
                {
                    playerName = "Ashley Young #17",
                    playerInfo = "Pos : Rightback \tAge: 32 \tCountry : England",
                    url = "http://www.futhead.com/18/players/6920/ashley-young/",
                    IconSource = ImageSource.FromFile("young.png"),

                },
                new templateMaker
                {
                    playerName = "Luke Shaw #23",
                    playerInfo = "Pos : Leftback \tAge: 22 \tCountry : England",
                    url = "http://www.futhead.com/18/players/3311/luke-shaw/",
                    IconSource = ImageSource.FromFile("shaw.png"),

                },
                new templateMaker
                {
                    playerName = "Matteo Darmian #",
                    playerInfo = "Pos : Rightback \tAge: 27 \tCountry : Italy",
                    url = "http://www.futhead.com/18/players/6020/matteo-darmian/",
                    IconSource = ImageSource.FromFile("darmian.png"),

                },
                new templateMaker
                {
                    playerName = "Daley Blind",
                    playerInfo = "Pos : Leftback \tAge: 27 \tCountry : Netherlands",
                    url = "http://www.futhead.com/18/players/4028/daley-blind/",
                    IconSource = ImageSource.FromFile("blind.png"),

                },
//MidFielder-----------------------------------------------------------------------------------
//Strikers-----------------------------------------------------------------------------------
                new templateMaker
                {
                    playerName = "Zlatan Ibrahimovic",
                    playerInfo = "Pos : Striker \tAge: 39 \tCountry : Sweden",
                    url = "http://www.futhead.com/18/players/49189/romelu-lukaku/",
                    IconSource = ImageSource.FromFile("ibra.png"),
                     
                },
                new templateMaker
                {
                    playerName = "Romelu Lukaku",
                    playerInfo = "Pos : Striker \tAge: 24 \tNation : Belgium",
                    url = "http://www.futhead.com/18/players/49189/romelu-lukaku/",
                    IconSource = ImageSource.FromFile("lukaku.png"),
                      
                },
            };
            SimpleListView.ItemsSource = listOfInts;
        }
    }
}
